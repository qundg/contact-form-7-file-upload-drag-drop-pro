To install this plugin see below:

Note : If you're currently using free version you must uninstall or delete it first before activating the pro. ( There are some issues if pro and free are both activated )

1. Upload the plugin files to the `/wp-content/plugins/drag-and-drop-upload-cf7-pro.zip` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress

Features

* Image Preview
* Auto Delete Files - After Submission
* Zip Files
* Save Files To Media Library
* Change Upload Directory
   - Generated Name (Timestamp)
   - Random Folder
   - By User
   - Custom Folder
* Email Attachment as Links or Zip Files
* Improved Security
* Optimized Code and Performance
* 1 Month Premium Support

For any bug or issues email me.

support email : glenmongaya@gmail.com