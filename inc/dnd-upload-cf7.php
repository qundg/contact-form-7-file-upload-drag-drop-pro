<?php

	/**
	* @Description : Plugin main core
	* @Package : Drag & Drop Multiple File Upload - Contact Form 7
	* @Author : CodeDropz
	*/

	if ( ! defined( 'ABSPATH' ) || ! defined('dnd_upload_cf7') || ! defined('dnd_upload_cf7_PRO') ) {
		exit;
	}

	/**
	* Begin : begin plugin initialization
	*/

	if( ! class_exists('CodeDropz_Drag_Drop_Upload_CF7') ) {

		class CodeDropz_Drag_Drop_Upload_CF7 {

			private static $instance = null;

			// default error message
			public $error_message = array();

			// Default upload options
			public $_options = array(
				'save_to_media'				=>	false,
				'automatic_file_deletion'	=>	false,
				'folder_option'				=>	null,
				'tmp_folder'				=>	'tmp_uploads',
				'upload_dir'				=>	null,
				'preview_image'				=>	'',
				'zip_files'					=>	false
			);

			public $counter = 0;

			// Upload dir - default from wp
			public $wp_upload_dir = array();

			/**
			* Creates or returns an instance of this class.
			*
			* @return  Init A single instance of this class.
			*/

			public static function get_instance() {
				if( null == self::$instance ) {
					self::$instance = new self;
				}
				return self::$instance;
			}

			/**
			* Load and initialize plugin
			*/

			private function __construct() {
				$this->init();
				$this->hooks();
				$this->filters();
			}

			/**
			* Plugin init
			*/

			public function init() {

				// Load plugin text domain
				$this->dnd_load_plugin_textdomain();

				// Wordpress upload directory
				$this->wp_upload_dir = wp_upload_dir();

				// Create upload folder where files being stored.
				if( defined('wp_dndcf7_upload_folder') ) {

					// concat path and defined folder dir
					$wp_dndcf7_folder = trailingslashit( $this->wp_upload_dir['basedir'] ) . wp_dndcf7_upload_folder;

					// Create dir
					if( ! is_dir( $wp_dndcf7_folder ) ) {
						wp_mkdir_p( $wp_dndcf7_folder );
					}

					// override default wordpress basedir and baseurl
					$this->wp_upload_dir['basedir'] = $wp_dndcf7_folder;
					$this->wp_upload_dir['baseurl'] = path_join( $this->wp_upload_dir['baseurl'] , wp_dndcf7_upload_folder );
				}

				// Set default error message
				$this->error_message = array(
					'server_limit'		=>	__('The uploaded file exceeds the maximum upload size of your server.','dnd-upload-cf7'),
					'failed_upload'		=>	__('Uploading a file fails for any reason','dnd-upload-cf7'),
					'large_file'		=>	__('Uploaded file is too large','dnd-upload-cf7'),
					'invalid_type'		=>	__('Uploaded file is not allowed for file type','dnd-upload-cf7'),
				);

				// check save to media - options
				$this->_options['save_to_media'] = get_option('drag_n_drop_save_to_media') ? true : false;

				// Auto delete file - options
				$this->_options['automatic_file_deletion'] = get_option('drag_n_drop_delete_options') ? true : false;

				// Image Preview - options
				$this->_options['preview_image'] = get_option('drag_n_drop_show_img_preview') ? true : false;

				// Compressed / Zipped files
				$this->_options['zip_files'] = get_option('drag_n_drop_zip_files') ? true : false;

				// Folder Option
				if( get_option('drag_n_drop_folder_option') ) {
					$this->_options['folder_option'] = get_option('drag_n_drop_folder_option');
				}

				// Setup upload path
				$_dir = '';

				// Switch folder options
				if( $this->_options['folder_option'] ) {
					switch(  $this->_options['folder_option'] ) {
						case 'date_n_time':
							$_dir = date('Y-m-d-h-i-s');
							break;
						case 'user_login':
							$current_user = wp_get_current_user();
							$_dir = ( ( is_user_logged_in() && $current_user->exists() ) ? 'user-'.$current_user->display_name : 'user-file-'.time() );
							break;
						case 'generated':
							$rand_max = mt_getrandmax();
							$_dir = zeroise( mt_rand( 0, $rand_max ), strlen( $rand_max ) );
							break;
						default:
							$_dir = ( get_option('drag_n_drop_custom_folder') ? get_option('drag_n_drop_custom_folder') : 'custom-' . date('m-d-y') );
							break;
					}
				}

				// Setup dir
				$this->_options['upload_dir'] = ( $_dir ? $_dir : $this->_options['tmp_folder'] );

			}

			/**
			* Begin : begin plugin hooks
			*/

			public function hooks() {

				add_action( 'wpcf7_init', array( $this, 'dnd_cf7_upload_add_form_tag_file') );
				add_action( 'wpcf7_admin_init', array( $this, 'dnd_upload_cf7_add_tag_generator'), 50 );
				add_action( 'wp_enqueue_scripts', array( $this, 'dnd_cf7_scripts') );

				// Ajax Upload
				add_action( 'wp_ajax_dnd_codedropz_upload', array( $this, 'dnd_upload_cf7_upload') );
				add_action( 'wp_ajax_nopriv_dnd_codedropz_upload', array( $this, 'dnd_upload_cf7_upload' ) );

				// Hook - Ajax Delete
				add_action('wp_ajax_nopriv_dnd_codedropz_upload_delete', array( $this,'dnd_codedropz_upload_delete') );
				add_action('wp_ajax_dnd_codedropz_upload_delete', array( $this, 'dnd_codedropz_upload_delete'));

				// Hook before mail send cf7
				add_action('wpcf7_before_send_mail', array( $this, 'dnd_cf7_before_send_mail'), 30, 1 );

				// Add Submenu - Settings
				add_action('admin_menu', array( $this, 'dnd_admin_settings') );

				// Hooks - Clean up files
				if( $this->_options['automatic_file_deletion'] === true ) {
					add_action('template_redirect', array( $this, 'wpcf7_remove_dnd_uploaded_files' ), 20, 3 );
				}
			}

			/**
			* Begin : Custom Cf7 filters
			*/

			private function filters() {

				// Add custom mime-type
				add_filter('upload_mimes', array( $this, 'dnd_extra_mime_types' ), 1, 1);

				// Encode type filter to support multipart since this is input type file
				add_filter( 'wpcf7_form_enctype',  array( $this, 'dnd_upload_cf7_form_enctype_filter') );

				// Validation + upload handling filter
				add_filter( 'wpcf7_validate_mfile',  array( $this, 'dnd_upload_cf7_validation_filter' ), 10, 2 );
				add_filter( 'wpcf7_validate_mfile*',  array( $this, 'dnd_upload_cf7_validation_filter' ), 10, 2 );

				// Add hidden fields
				add_filter('wpcf7_form_hidden_fields', function( $hidden ){
					return array_merge( $hidden, array(
						'upload_dir'		=>	$this->_options['upload_dir'],
						'generate_name'		=>	date('m-d-y') . '-'. uniqid()
					));
				});

				// Modify email body
				add_filter( 'dnd_wpcf7_email_body', array( $this, 'dnd_upload_cf7_email_body' ), 10, 2 );

				// filter before mail send
				add_filter('wpcf7_mail_components', array( $this, 'dnd_cf7_mail_components'), 50, 2 );
			}

			/**
			* Delete specific files
			*/

			public function dnd_codedropz_upload_delete() {

				// Make sure path is set
				if( isset( $_POST['path'] ) && ! empty( $_POST['path'] ) ) {
					$file_path = $this->wp_upload_dir['basedir'] . trim( $_POST['path'] );
					if( file_exists( $file_path ) ){
						wp_delete_file( $file_path );
						wp_send_json_success( true );
					}
				}

				die;
			}


			/**
			* Clean file / auto delete files
			*/

			public function wpcf7_remove_dnd_uploaded_files( $dir_path, $seconds = 600, $max = 60 ) {
				if ( is_admin() || 'GET' != $_SERVER['REQUEST_METHOD'] || is_robots() || is_feed() || is_trackback() ) {
					return;
				}

				// Setup dirctory path
				$dir = ( ! $dir_path  ? trailingslashit( $this->wp_upload_dir['basedir'] ) : trailingslashit( $dir_path ) );

				// Make sure dir is readable or writable
				if ( ! is_dir( $dir ) || ! is_readable( $dir ) || ! wp_is_writable( $dir ) ) {
					return;
				}

				$seconds = absint( get_option('drag_n_drop_delete_options') ? ( (int) get_option('drag_n_drop_delete_options') * 60 * 60 ) : $seconds );
				$max = absint( $max );
				$count = 0;

				if ( $handle = @opendir( $dir ) ) {
					while ( false !== ( $file = readdir( $handle ) ) ) {
						if ( $file == "." || $file == ".." ) {
							continue;
						}

						// Setup dir and filename
						$file_path = $dir . $file;

						// Check if current path is directory (recursive)
						if( is_dir( $file_path ) ) {
							$this->wpcf7_remove_dnd_uploaded_files( $file_path );
							continue;
						}

						// Get file time of files OLD files.
						$mtime = @filemtime( $file_path );

						if ( $mtime && time() < $mtime + $seconds ) { // less than $seconds old
							continue;
						}

						// Delete files from dir
						wp_delete_file( $file_path );

						$count += 1;

						if ( $max <= $count ) {
							break;
						}
					}
					@closedir( $handle );
				}
				@rmdir( $dir );
			}

			/**
			* Encode type filter to support multipart since this is input type file
			*/

			public function dnd_upload_cf7_form_enctype_filter( $enctype ) {
				$multipart = (bool) wpcf7_scan_form_tags( array( 'type' => array( 'drag_drop_file', 'drag_drop_file*' ) ) );

				if ( $multipart ) {
					$enctype = 'multipart/form-data';
				}

				return $enctype;
			}

			/**
			* Validation + upload handling filter
			*/

			public function dnd_upload_cf7_validation_filter( $result, $tag ) {

				// Setup cf7 files tags name & option
				$name = $tag->name;
				$id = $tag->get_id_option();

				$multiple_files = ( isset( $_POST[ $name ] ) ? $_POST[ $name ] : null );

				// Check if we have files or if it's empty
				if( ( is_null( $multiple_files ) || count( $multiple_files ) == 0 ) && $tag->is_required() ) {
					$result->invalidate( $tag, wpcf7_get_message( 'invalid_required' ) );
					return $result;
				}

				// If option Zip Files & Move to media library
				if ( $submission = WPCF7_Submission::get_instance() ) {

					// Make sure there's no error on form before we take any action.
					if( count( $result->get_invalid_fields() ) == 0 ) {

						// Get all attachment
						$post_files = $_POST[ $name ];

						// If save to media and not zip ( Move from /tmp_uploads to uploads/2019/08  )
						if( $this->_options['save_to_media'] === true && $this->_options['zip_files'] !== true ) {
							$response = array();

							// Loop each attachment ( before moving files to upl )
							foreach( $post_files as $file ) {
								$file_url = $this->wp_upload_dir['baseurl'] . $file;
								//@todo - parse response error/success
								$this->dnd_save_to_media_library( $file_url );
							}
						}

						// if zip files is check ( Zip files when user click submit )
						if( $this->_options['zip_files'] === true ) {

							// Make sure we have files before we proceed
							if( count( $post_files ) > 0 ) {

								// Compressed files @return array
								$files = $this->dnd_zip_all_files( $post_files );

								// Check if file compression is successfull`
								if( $files === false ) {
									$result->invalidate( $tag, __('Error : Unable to zip files.','dnd-upload-cf7') );
									return $result;
								}

								// If save to media option is set to yes
								if( $this->_options['save_to_media'] === true ) {
									$response = $this->dnd_save_to_media_library( $files );
									if( isset( $response['error'] ) && ! empty( $response['error'] ) ) {
										$result->invalidate( $tag, $response['error'] );
										return $result;
									}
								}
							}
						}
					}
				}

				return $result;
			}

			/**
			* Add custom mime-types
			*/

			public function dnd_extra_mime_types( $mime_types ){
				$mime_types['xls'] = 'application/excel, application/vnd.ms-excel, application/x-excel, application/x-msexcel';
				$mime_types['xlsx'] = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
				return $mime_types;
			}

			/**
			* Register form tag(mfile - file)
			*/

			public function dnd_cf7_upload_add_form_tag_file() {
				wpcf7_add_form_tag(	array( 'mfile ', 'mfile*'), array( $this, 'dnd_cf7_upload_form_tag_handler'), array( 'name-attr' => true ) );
			}

			/**
			* Begin : Generate Tags
			*/

			public function dnd_upload_cf7_add_tag_generator() {
				$tag_generator = WPCF7_TagGenerator::get_instance();
				$tag_generator->add( 'upload-file', __( 'multiple file upload', 'dnd-upload-cf7' ), array( $this, 'dnd_upload_cf7_tag_generator_file' ) );
			}

			/**
			* Begin : Display Form In wp-admin
			*/

			public function dnd_upload_cf7_tag_generator_file( $contact_form, $args = '' ) {

				// Parse data and get our options
				$args = wp_parse_args( $args, array() );

				// Our multiple upload field
				$type = 'mfile';

				$description = __( "Generate a form-tag for a file uploading field. For more details, see %s.", 'contact-form-7' );
				$desc_link = wpcf7_link( __( 'https://contactform7.com/file-uploading-and-attachment/', 'contact-form-7' ), __( 'File Uploading and Attachment', 'contact-form-7' ) );

				if( file_exists( dnd_upload_cf7_directory .'/inc/admin/admin-cf7-fields.php' ) ) {
					include_once( wp_normalize_path( dnd_upload_cf7_directory .'/inc/admin/admin-cf7-fields.php' ) );
				}
			}

			/**
			* Begin : Form tag handler from the tag - callback
			*/

			public function dnd_cf7_upload_form_tag_handler( $tag ) {

				// check and make sure tag name is not empty
				if ( empty( $tag->name ) ) {
					return '';
				}

				// Validate our fields
				$validation_error = wpcf7_get_validation_error( $tag->name );

				// Generate class
				$class = wpcf7_form_controls_class( 'drag-n-drop-file d-none' );

				// Add not-valid class if there's an error.
				if ( $validation_error ) {
					$class .= ' wpcf7-not-valid';
				}

				// Setup element attributes
				$atts = array();

				$atts['size'] = $tag->get_size_option( '40' );
				$atts['class'] = $tag->get_class_option( $class );
				$atts['id'] = $tag->get_id_option();
				$atts['tabindex'] = $tag->get_option( 'tabindex', 'signed_int', true );

				// If file is required
				if ( $tag->is_required() ) {
					$atts['aria-required'] = 'true';
				}

				// Set invalid attributes if there's validation error
				$atts['aria-invalid'] = $validation_error ? 'true' : 'false';

				// Set input type and name
				$atts['type'] = 'file';
				$atts['multiple'] = 'multiple';
				$atts['data-name'] = $tag->name;
				$atts['data-type'] = $tag->get_option( 'filetypes','', true);
				$atts['data-limit'] = $tag->get_option( 'limit','', true);
				$atts['data-max'] = $tag->get_option( 'max-file','', true);

				// Combine and format attrbiutes
				$atts = wpcf7_format_atts( $atts );

				// Return our element and attributes
				return sprintf('<span class="wpcf7-form-control-wrap %1$s"><input %2$s />%3$s</span>',	sanitize_html_class( $tag->name ), $atts, $validation_error );
			}

			/**
			* Filter to modify body contents ( Only Attachment Links )
			*/

			public function dnd_upload_cf7_email_body( $files = array(), $mail ) {

				if( empty( $files ) ) {
					return array();
				}

				$links = array();

				// Get upload directory
				$upload_dir = trim( $_POST['upload_dir'] );

				// zip files
				if( $this->_options['zip_files'] === true ) {
					$generated_name = trim( $_POST['generate_name'] ) . '.zip';
					$zip_file = trailingslashit( $this->wp_upload_dir['baseurl'] ) . $upload_dir .'/'. $generated_name;
					if( $this->_options['save_to_media'] === true ) {
						$zip_file = trailingslashit( $this->wp_upload_dir['url'] ) . $generated_name;
					}
					$links[] = ( $mail['use_html'] ? '<a target="_blank" href="'. esc_url( $zip_file ) .'">'. esc_html( $zip_file ) .'</a>' : esc_html( $zip_file ) );
				}else {
					foreach( $files as $_file ) {
						$file_link = $this->wp_upload_dir['baseurl'] . $_file;
						if( $this->_options['save_to_media'] === true ) {
							$file_link = trailingslashit( $this->wp_upload_dir['url'] ) . basename( $_file );
						}
						$links[] = ( $mail['use_html'] ? '<a target="_blank" href="'. esc_url( $file_link ).'">- '.  esc_html( $file_link ) .'</a>' : '-' . esc_html( $file_link ) );
					}
				}

				// Make a list
				if( count( $links ) > 0 ) {
					return $links;
				}
			}

			/**
			* Begin process ajax upload.
			*/

			public function dnd_upload_cf7_upload() {

				// Make sure upload directory is set
				if( ! empty( $_POST['upload_dir'] ) ) {
					// Set Directory/Path - where the files being stored.
					$this->_options['upload_dir'] = path_join( $this->wp_upload_dir['basedir'], trim( $_POST['upload_dir'] ) );
				}

				// input type file 'name'
				$name = 'upload-file';

				// Setup $_FILE name (from Ajax)
				$file = isset( $_FILES[$name] ) ? $_FILES[$name] : null;

				// Tells whether the file was uploaded via HTTP POST
				if ( ! is_uploaded_file( $file['tmp_name'] ) ) {
					wp_send_json_error( get_option('drag_n_drop_error_failed_to_upload') ? get_option('drag_n_drop_error_failed_to_upload') : $this->get_error_msg('failed_upload') );
				}

				/* File type validation */
				$file_type_pattern = $this->dnd_upload_cf7_filetypes( $_POST['supported_type'] );

				// validate file type
				if ( ! preg_match( $file_type_pattern, $file['name'] ) ) {
					wp_send_json_error( get_option('drag_n_drop_error_invalid_file') ? get_option('drag_n_drop_error_invalid_file') : $this->get_error_msg('invalid_type') );
				}

				// validate file size limit
				if( $file['size'] > (int)$_POST['size_limit'] ) {
					wp_send_json_error( get_option('drag_n_drop_error_files_too_large') ? get_option('drag_n_drop_error_files_too_large') : $this->get_error_msg('large_file') );
				}

				// Temporary TMP folder/dir
				$tmp_dir = path_join( $this->wp_upload_dir['basedir'], $this->_options['tmp_folder'] );

				// Get dir setup
				$base_dir = $this->dnd_get_dir_setup( $tmp_dir, true );

				// Create file name
				$filename = $file['name'];
				$filename = wpcf7_canonicalize( $filename, 'as-is' );
				$filename = wpcf7_antiscript_file_name( $filename );

				// Add filter on upload file name
				$filename = apply_filters( 'wpcf7_upload_file_name', $filename,	$file['name'] );

				// Generate new filename
				$filename = wp_unique_filename( $base_dir, $filename );
				$new_file = path_join( $base_dir, $filename );

				// Upload Files to Temporary Folder ( Only Files - NOT ZIP )
				if( $this->_options['zip_files'] !== true && $this->_options['save_to_media'] === true ) {
					$new_file = path_join( $tmp_dir, $filename );
				}

				// Php manual files upload
				if ( false === move_uploaded_file( $file['tmp_name'], $new_file ) ) {
					$error_upload = get_option('drag_n_drop_error_failed_to_upload') ? get_option('drag_n_drop_error_failed_to_upload') : $this->get_error_msg('failed_upload');
					wp_send_json_error( $error_upload );
				}else{

					// Setup path and file name and add it to response.
					$path = trailingslashit( '/' . basename( $base_dir ) );

					// Get details of attachment from media_json_respons function
					$files = $this->_media_json_response( $path, $filename );

					// Change file permission to 0400
					chmod( $new_file, 0644 );

					wp_send_json_success( $files );
				}

				die;
			}

			/**
			* Hooks before sending mail / format url/links ( Append links to mail body )
			*/

			public function dnd_cf7_before_send_mail( $wpcf7 ){
				global $_mail;

				// Mail Counter
				$_mail = 0;

				// cf7 instance
				$submission = WPCF7_Submission::get_instance();

				// Check for submission
				if( $submission ) {

					// Get posted data
					$submitted['posted_data'] = $submission->get_posted_data();

					// Parse fields
					$fields = $wpcf7->scan_form_tags();

					// Prop email
					$mail = $wpcf7->prop('mail');
					$mail_2 = $wpcf7->prop('mail_2');

					// An array contents file full path.
					$files = '';

					// Loop fields and replace mfile code
					foreach( $fields as $field ) {
						if( $field->basetype == 'mfile') {
							if( isset( $submitted['posted_data'][$field->name] ) && ! empty( $submitted['posted_data'][$field->name] ) ) {

								// Check If not send attachment as links then empty body.
								if( get_option('drag_n_drop_mail_attachment') !== 'yes' ) {
									$mail['body'] = str_replace( "[$field->name]", "", $mail['body'] );
									if( $mail_2['active'] ) {
										$mail_2['body'] = str_replace( "[$field->name]", "", $mail_2['body'] );
									}
								}else {

									// Allow plugins to override and modify email body contents
									if( is_array( $submitted['posted_data'][$field->name] ) && count( $submitted['posted_data'][$field->name] ) > 0 )
										$files = apply_filters('dnd_wpcf7_email_body', $submitted['posted_data'][$field->name], $mail );

									// Setup mail body with links
									if( $files && count( $files ) > 0 ) {
										$mail['body'] = str_replace( "[$field->name]", "\n" . implode( "\n", $files ), $mail['body'] );
										if( $mail_2['active'] ) {
											$mail_2['body'] = str_replace( "[$field->name]", "\n" . implode( "\n", $files ), $mail_2['body'] );
										}
									}
								}
							}
						}
					}

					// Save the email body
					$wpcf7->set_properties( array("mail" => $mail) );

					// if mail 2
					if( $mail_2['active'] ) {
						$wpcf7->set_properties( array("mail_2" => $mail_2) );
					}
				}

				return $wpcf7;
			}

			/**
			* filter - Custom cf7 Mail components ( For attachment - only )
			*/

			public function dnd_cf7_mail_components( $components, $form ) {
				global $_mail;

				// Send email link as an attachment.
				if( get_option('drag_n_drop_mail_attachment') == 'yes' ) {
					return $components;
				}

				// cf7 - Submission Object
				$submission = WPCF7_Submission::get_instance();

				// get all form fields
				$fields = $form->scan_form_tags();

				// If mail_2 is set - Do not send attachment ( unless File Attachment field is not empty )
				if( ( $mail_2 = $form->prop('mail_2') ) && $mail_2['active'] && empty( $mail_2['attachments'] ) && $_mail >= 1 ) {
					return $components;
				}

				// Loop fields get mfile only.
				foreach( $fields as $field ) {

					// If field type equal to mfile which our default field.
					if( $field->basetype == 'mfile') {

						// Make sure we have files to attach
						if( isset( $_POST[ $field->name ] ) && count( $_POST[ $field->name ] ) > 0 ) {

							// zip files only
							if( $this->_options['zip_files'] === true ) {
								$generated_name = trim( $_POST['generate_name'] ) . '.zip';
								$components['attachments'][0] = trailingslashit( $this->wp_upload_dir['basedir'] ) . trim( $_POST['upload_dir'] ) .'/'. $generated_name;
								if( $this->_options['save_to_media'] === true ) {
									$components['attachments'][0] = trailingslashit( $this->wp_upload_dir['path'] ) . $generated_name;
								}

							}else {
								// Loop all the files and attach to cf7 components
								foreach( $_POST[ $field->name ] as $_file ) {
									// Join dir and a new file name ( get from <input type="hidden" name="upload-file-333"> )
									$new_file_name = trailingslashit( $this->wp_upload_dir['basedir'] ) . $_file;
									if( $this->_options['save_to_media'] === true ) {
										$new_file_name = trailingslashit( $this->wp_upload_dir['path'] ) . basename( $_file );
									}
									// Check if submitted and file exists then file is ready.
									if ( $submission && file_exists( $new_file_name )) {
										$components['attachments'][] = $new_file_name;
									}
								}
							}

						}
					}
				}

				// Increment mail counter
				$_mail = $_mail + 1;

				// Return setup components
				return $components;
			}

			/**
			* Get dir zip setup
			*/

			public function dnd_get_dir_setup( $directory = null, $create = false ) {

				// check send as links attachment settings & zip file option
				if( get_option('drag_n_drop_mail_attachment') == 'yes' || ( get_option('drag_n_drop_mail_attachment') == 'yes' && $this->_options['zip_files'] === true ) ) {
					$directory = $this->_options['upload_dir'];
				}

				// Create dir
				if( $create ) {
					if( ! is_dir( $directory ) ) {
						wp_mkdir_p( $directory );
					}
					if( file_exists( $directory ) ) {
						return $directory;
					}
				}

				// Get current IDR
				return $directory;
			}

			/**
			* Begin : Load js and css
			*/

			public function dnd_cf7_scripts() {
				// Get plugin version
				$version = dnd_upload_cf7_version;

				// enque script
				wp_enqueue_script( 'codedropz-uploader', plugins_url ('/assets/js/codedropz-uploader-min.js', dirname(__FILE__) ), array('jquery','contact-form-7'), $version, true );
				wp_enqueue_script( 'dnd-upload-cf7', plugins_url ('/assets/js/dnd-upload-cf7.js', dirname(__FILE__) ), array('jquery','codedropz-uploader','contact-form-7'), $version, true );

				//  registered script with data for a JavaScript variable.
				wp_localize_script( 'dnd-upload-cf7', 'dnd_cf7_uploader',
					array(
						'ajax_url' 				=> admin_url( 'admin-ajax.php' ),
						'drag_n_drop_upload' 	=> array(
							'text'				=>	( get_option('drag_n_drop_text') ? get_option('drag_n_drop_text') : __('Drag & Drop Files Here','dnd-upload-cf7') ),
							'or_separator'		=>	( get_option('drag_n_drop_separator') ? get_option('drag_n_drop_separator') : __('or','dnd-upload-cf7') ),
							'browse'			=>	( get_option('drag_n_drop_browse_text') ? get_option('drag_n_drop_browse_text') : __('Browse Files','dnd-upload-cf7') ),
							'server_max_error'	=>	( get_option('drag_n_drop_error_server_limit') ? get_option('drag_n_drop_error_server_limit') : $this->get_error_msg('server_limit') ),
							'large_file'		=>	( get_option('drag_n_drop_error_files_too_large') ? get_option('drag_n_drop_error_files_too_large') : $this->get_error_msg('large_file') ),
							'inavalid_type'		=>	( get_option('drag_n_drop_error_invalid_file') ? get_option('drag_n_drop_error_invalid_file') : $this->get_error_msg('invalid_type') ),
						)
					)
				);

				// enque style
				wp_enqueue_style( 'dnd-upload-cf7', plugins_url ('/assets/css/dnd-upload-cf7.css', dirname(__FILE__) ), '', $version );
			}

			/**
			* Upload File and Save to Media Libray
			*/

			public function dnd_save_to_media_library( $files = array() ) {

				// Response status
				$response = array();

				// Required wordpress file and image library
				if ( ! function_exists( 'media_handle_upload' ) ) {
					require_once( ABSPATH . 'wp-admin/includes/file.php' );
					require_once( ABSPATH . 'wp-admin/includes/image.php' );
					require_once( ABSPATH . 'wp-admin/includes/media.php' );
				}

				// Check if $_Files is not empty
				if( empty( $files ) ) {
					$response['error'] = ( get_option('drag_n_drop_error_failed_to_upload') ? get_option('drag_n_drop_error_failed_to_upload') : $this->get_error_msg('failed_upload') );
				}

				// Downlod tmp files
				$tmp = download_url( $files );

				$file_array = array(
					'name' => basename( $files ),
					'tmp_name' => $tmp
				);

				// remove tmp files if it's invalid
				if ( is_wp_error( $tmp ) ) {
					@unlink( $file_array[ 'tmp_name' ] );
				}

				// Insert media files
				$attachment_id = media_handle_sideload( $file_array, 0 );

				// Remove files
				if ( is_wp_error( $attachment_id ) ) {
					@unlink( $file_array['tmp_name'] );
				}

				// // Check status of upload if it's success or not
				if ( ! is_wp_error( $attachment_id ) ) {

					// Setup path and file name and add it to response.
					$path = trailingslashit( $this->wp_upload_dir['subdir'] );

					// get attachment meta data
					$file_name['file'] = wp_get_attachment_url( $attachment_id );

					// Get details of attachment from media_json_respons function
					$media_files = $this->_media_json_response( $path, basename( $file_name['file'] ) );

					// Assign response
					$response['success'] = $media_files;

					// Delete files from tmp_folder
					$zip_path =  $this->wp_upload_dir['basedir'] .'/'. trailingslashit( $this->_options['tmp_folder'] ) . basename( $file_name['file'] );
					if( file_exists( $zip_path ) ) {
						wp_delete_file( $zip_path );
					}

				}else {
					$response['error'] = ( get_option('drag_n_drop_error_failed_to_upload') ? get_option('drag_n_drop_error_failed_to_upload') : $attachment_id->get_error_message() );
				}

				//@todo : Remove files from tmp_folders

				return $response;
			}

			/**
			* Setup and Zip Attachment
			*/

			public function dnd_zip_all_files( $files ) {

				// Make sure we have files
				if( empty( $files ) ) {
					return false;
				}

				// Default dir/path
				$default_dir = $_POST['upload_dir'];
				$dir_path = path_join( $this->wp_upload_dir['basedir'], $default_dir );

				// Get file dir setup if zip is check but not send attachment as links.
				if( $this->_options['zip_files'] === true && get_option('drag_n_drop_mail_attachment') !== 'yes' ) {
					$default_dir = $this->_options['tmp_folder'];
					$dir_path = path_join( $this->wp_upload_dir['basedir'], $this->_options['tmp_folder'] );
				}

				// Setup dir and begin to create .zip file
				$zip = new ZipArchive;

				// zip generated name
				$generate_name = trim( $_POST['generate_name'] );

				// new zip name
				$zip_name = trailingslashit( $dir_path ) . $generate_name . '.zip';

				// check if zip files already created.
				$exists = ( file_exists( $zip_name ) ? ZipArchive::OVERWRITE : ZipArchive::CREATE );

				// Open zip file
				if ( $zip_open = $zip->open( $zip_name , $exists ) === TRUE ) {
					foreach( $files as $file ) {
						// zip only the files that are exists.
						$for_zip_file = $this->wp_upload_dir['basedir'] . $file;
						if( file_exists( $for_zip_file ) ) {
							$zip->addFile( $for_zip_file , basename( $for_zip_file ) );
						}
					}
				}

				// Closing zip
				$zip->close();

				// Delete temporary files
				$this->dnd_delete_wp_files( $files );

				// Return whole path of zip
				if( $zip_open && file_exists( $zip_name ) ) {
					return trailingslashit( $this->wp_upload_dir['baseurl'] ) . trailingslashit( $default_dir ) . basename( $zip_name );
				}else {
					return false;
				}
			}

			/**
			* Unlink or delete files
			*/

			public function dnd_delete_wp_files( $files ) {
				if( count( $files ) > 0 ) {
					foreach( $files as $_file ) {
						wp_delete_file( $this->wp_upload_dir['basedir'] . $_file );
					}
				}
			}

			/**
			* Setup media file on json response after the successfull upload.
			*/

			public function _media_json_response( $path, $file_name ) {
				$preview = false;

				// Apply preview on images only
				if( $this->_options['preview_image'] ) {
					if ( preg_match( '/\.(jpg|jpeg|png|gif|tiff|ai|eps|pdf|ai|svg|psd )$/i', $file_name ) ) {
						$preview = $this->wp_upload_dir['baseurl'] . $path . $file_name;
					}
				}

				$media_files = array(
					'path'		=>	$path,
					'file'		=>	$file_name,
					'preview'	=>	$preview
				);

				return $media_files;
			}

			/**
			* Setup file type pattern for validation
			*/

			public function dnd_upload_cf7_filetypes( $types ) {
				$file_type_pattern = '';

				// If contact form 7 5.0 and up
				if( function_exists('wpcf7_acceptable_filetypes') ) {
					$file_type_pattern = wpcf7_acceptable_filetypes( $types, 'regex' );
					$file_type_pattern = '/\.(' . $file_type_pattern . ')$/i';
				}else{
					$allowed_file_types = array();
					$file_types = explode( '|', $types );

					foreach ( $file_types as $file_type ) {
						$file_type = trim( $file_type, '.' );
						$file_type = str_replace( array( '.', '+', '*', '?' ), array( '\.', '\+', '\*', '\?' ), $file_type );
						$allowed_file_types[] = $file_type;
					}

					$allowed_file_types = array_unique( $allowed_file_types );
					$file_type_pattern = implode( '|', $allowed_file_types );

					$file_type_pattern = trim( $file_type_pattern, '|' );
					$file_type_pattern = '(' . $file_type_pattern . ')';
					$file_type_pattern = '/\.' . $file_type_pattern . '$/i';
				}

				return $file_type_pattern;
			}

			/**
			* create admin settings
			*/

			public function dnd_admin_settings() {
				add_submenu_page( 'wpcf7', 'Drag & Drop Uploader - Settings', 'Drag & Drop Upload', 'manage_options', 'drag-n-drop-upload',array( $this, 'dnd_upload_admin_settings') );
				add_action('admin_init', array( $this, 'dnd_upload_register_settings') );
			}

			/**
			* Callable functions display HTML admin settings
			*/

			public function dnd_upload_admin_settings( ) {
				if( file_exists( dnd_upload_cf7_directory .'/inc/admin/dnd-admin-settings.php' ) ) {
					include_once( wp_normalize_path( dnd_upload_cf7_directory .'/inc/admin/dnd-admin-settings.php' ) );
				}
			}

			/**
			* Default error message
			*/

			public function get_error_msg( $error_key ) {
				// return error message based on $error_key request
				if( isset( $this->error_message[$error_key] ) ) {
					return $this->error_message[$error_key];
				}
				return false;
			}

			/**
			* Load plugin text-domain
			*/

			public function dnd_load_plugin_textdomain() {
				load_plugin_textdomain( 'dnd-upload-cf7', false, dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages' );
			}

			/**
			* Set Custom Cookie
			* @ param : name(string), value(array), expire(2hours)
			*/

			private static function set_cookie( $name, $value, $expire = 7200 ) {
				setcookie( $name, ( is_array( $value ) ? maybe_serialize( $value ) : $value ), time() + $expire, COOKIE_DOMAIN, is_ssl() );
			}

			/**
			* Get Cookie
			* @ param : name (string)
			* @ return : array or string
			*/

			private static function get_cookie( $name ) {
				if( isset( $_COOKIE[ $name ] ) ) {
					return ( is_serialized( $_COOKIE[ $name ] ) ? maybe_unserialize( $_COOKIE[ $name ] ) : $_COOKIE[ $name ] );
				}
				return false;
			}

			/**
			* Delete Cookie
			* @ param : name(string)
			*/

			private static function delete_cookie( $name ) {
				if( isset( $_COOKIE[ $name ] ) ) {
					unset( $_COOKIE[ $name ] );
					self::set_cookie( $name, '', -1 );
				}
			}

			/**
			* Save admin settings
			*/

			public function dnd_upload_register_settings() {

				$settings = array(
					'drag_n_drop_mail_attachment',
					'drag_n_drop_text',
					'drag_n_drop_separator',
					'drag_n_drop_browse_text',
					'drag_n_drop_error_server_limit',
					'drag_n_drop_error_failed_to_upload',
					'drag_n_drop_error_files_too_large',
					'drag_n_drop_error_invalid_file',
					'drag_n_drop_one_file_at_time',
					'drag_n_drop_save_to_media',
					'drag_n_drop_folder_option',
					'drag_n_drop_custom_folder',
					'drag_n_drop_delete_options',
					'drag_n_drop_show_img_preview',
					'drag_n_drop_zip_files'
				);

				foreach( $settings as $option ) {
					register_setting( 'drag-n-drop-upload-file-cf7', $option );
				}
			}
		}
	}

	/**
	* Initialize using singleton pattern
	*/

	function codeDropz_dnd_upload_cf7() {
		$CodeDropz = CodeDropz_Drag_Drop_Upload_CF7::get_instance();
		return $CodeDropz;
	}

	/**
	* Add hooks on ajax response ( Contact Form 7 - API )
	*/

	add_filter('wpcf7_ajax_json_echo', 'wpcf_dnd_ajax_json_response',20,2);
	function wpcf_dnd_ajax_json_response( $response, $result ){
		if( $result['status'] != 'validation_failed' ) {
			$codDropz = codeDropz_dnd_upload_cf7();
			$response['drag_n_drop'] = array(
				'upload_dir'		=>	$codDropz->_options['upload_dir'],
				'generate_name'		=>	date('m-d-y') . '-'. uniqid()
			);
		}
		return $response;
	}

	// Launch the whole plugin.
	add_action( 'plugins_loaded', 'codeDropz_dnd_upload_cf7' );