<?php
	/**
	* @Description : Admin Settings - PRO
	* @Package : Drag & Drop Multiple File Upload - Contact Form 7
	* @Author : CodeDropz
	*/

	/**  This protect the plugin file from direct access */
	if ( ! defined( 'ABSPATH' ) || ! defined('dnd_upload_cf7') || ! defined('dnd_upload_cf7_PRO') ) {
		exit;
	}

	// show upload dir/folder table options
	$show_upload = ( ( ! get_option('drag_n_drop_save_to_media') && get_option('drag_n_drop_mail_attachment') == 'yes' ) ? 'show' : '' );
	$zip_error = ( ! class_exists('ZipArchive') ? __("Your server Doens't support Zip (ZipArchive need to be enable, contact your hosting provider)",'dnd-upload-cf7') : false );
?>
	<div class="wrap">
		<h1><?php _e('Drag & Drop Uploader - Settings','dnd-upload-cf7'); ?></h1>

		<?php settings_errors(); ?>

		<form method="post" action="options.php">

			<?php
				settings_fields( 'drag-n-drop-upload-file-cf7' );
				do_settings_sections( 'drag-n-drop-upload-file-cf7' );
			?>

			<h2><?php _e('Uploader Info','dnd-upload-cf7'); ?></h2>

			<table class="form-table">
				<tr valign="top">
					<th scope="row"><?php _e('Drag & Drop Text','dnd-upload-cf7'); ?></th>
					<td><input type="text" name="drag_n_drop_text" class="regular-text" value="<?php echo esc_attr( get_option('drag_n_drop_text') ); ?>" placeholder="Drag & Drop Files Here" /></td>
				</tr>
				<tr valign="top">
					<th scope="row"></th>
					<td><input type="text" name="drag_n_drop_separator" value="<?php echo esc_attr( get_option('drag_n_drop_separator') ); ?>" placeholder="or" /></td>
				</tr>
				<tr valign="top">
					<th scope="row"><?php _e('Browse Text','dnd-upload-cf7'); ?></th>
					<td><input type="text" name="drag_n_drop_browse_text" class="regular-text" value="<?php echo esc_attr( get_option('drag_n_drop_browse_text') ); ?>" placeholder="Browse Files" /></td>
				</tr>
			</table>

			<h2><?php _e('Error Message','dnd-upload-cf7'); ?></h2>

			<table class="form-table">
				<tr valign="top">
					<th scope="row"><?php _e('File exceeds server limit','dnd-upload-cf7'); ?></th>
					<td><input type="text" name="drag_n_drop_error_server_limit" class="regular-text" value="<?php echo esc_attr( get_option('drag_n_drop_error_server_limit') ); ?>" placeholder="<?php echo $this->get_error_msg('server_limit'); ?>" /></td>
				</tr>
				<tr valign="top">
					<th scope="row"><?php _e('Failed to Upload','dnd-upload-cf7'); ?></th>
					<td><input type="text" name="drag_n_drop_error_failed_to_upload" class="regular-text" value="<?php echo esc_attr( get_option('drag_n_drop_error_failed_to_upload') ); ?>" placeholder="<?php echo $this->get_error_msg('failed_upload'); ?>" /></td>
				</tr>
				<tr valign="top">
					<th scope="row"><?php _e('Files too large','dnd-upload-cf7'); ?></th>
					<td><input type="text" name="drag_n_drop_error_files_too_large" class="regular-text" value="<?php echo esc_attr( get_option('drag_n_drop_error_files_too_large') ); ?>" placeholder="<?php echo $this->get_error_msg('large_file'); ?>" /></td>
				</tr>
				<tr valign="top">
					<th scope="row"><?php _e('Invalid file Type','dnd-upload-cf7'); ?></th>
					<td><input type="text" name="drag_n_drop_error_invalid_file" class="regular-text" value="<?php echo esc_attr( get_option('drag_n_drop_error_invalid_file') ); ?>" placeholder="<?php echo $this->get_error_msg('invalid_type'); ?>" /></td>
				</tr>
			</table>

			<h2><?php _e('**Pro Features','dnd-upload-cf7'); ?></h2>

			<table class="form-table">
				<tr valign="top">
					<th scope="row"><?php _e('Send file(s) as links?','dnd-upload-cf7'); ?> </th>
					<td><input name="drag_n_drop_mail_attachment" class="conditional-field" type="checkbox" value="yes" <?php checked('yes', get_option('drag_n_drop_mail_attachment')); ?>> Yes <span class="description"><?php _e('( Files will be saved on your server )','dnd-upload-cf7'); ?><span><br>
					<p class="description"><?php _e('Display links on email and redirect to the attachment.','dnd-upload-cf7'); ?></p></td>
				</tr>
				<tr valign="top" style="display:none">
					<th scope="row"><?php _e('Upload (1) file at a Time','dnd-upload-cf7'); ?></th>
					<td><input name="drag_n_drop_one_file_at_time" type="checkbox" <?php checked( get_option('drag_n_drop_one_file_at_time'),1 ); ?> value="1"> Yes - <span class="description"><?php _e('This will save time and prevent multiple server request','dnd-upload-cf7'); ?></span></td>
				</tr>
				<tr valign="top">
					<th scope="row"><?php _e('Save Files to Media Library?','dnd-upload-cf7'); ?></th>
					<td><input name="drag_n_drop_save_to_media" class="conditional-field-media" <?php checked( get_option('drag_n_drop_save_to_media'),1 ); ?> type="checkbox" value="1"> Yes</td>
				</tr>
				<tr valign="top" class="conditional-show <?php echo $show_upload; ?>">
					<th scope="row"><?php _e('Upload Folder','dnd-upload-cf7'); ?></th>
					<td>
						<fieldset>
							<label><input type="radio" name="drag_n_drop_folder_option" <?php checked( get_option('drag_n_drop_folder_option'),'date_n_time' ); ?> value="date_n_time"><span><?php _e('Generated Date & Time','dnd-upload-cf7'); ?></span> <code><?php echo date('Y'); ?>-01-14-14-22-58</code></label><br>
							<label><input type="radio" name="drag_n_drop_folder_option" <?php checked( get_option('drag_n_drop_folder_option'), 'generated' ); ?> value="generated"><span><?php _e('Random Folder','dnd-upload-cf7'); ?></span> <code>11ol987xJyd12</code></label><br>
							<label><input type="radio" name="drag_n_drop_folder_option" <?php checked( get_option('drag_n_drop_folder_option'), 'user_login' ); ?> value="user_login"><span><?php _e('By User','dnd-upload-cf7'); ?></span> <code>user-john-smith</code></label><span class="description"> <?php _e('( User must login )','dnd-upload-cf7'); ?></span><br>
							<label><input type="radio" name="drag_n_drop_folder_option" <?php checked( get_option('drag_n_drop_folder_option'), 'custom_folder' ); ?> value="custom_folder"><span><?php _e('Custom Folder','dnd-upload-cf7'); ?> :</span>
							<input type="text" name="drag_n_drop_custom_folder" value="<?php echo get_option('drag_n_drop_custom_folder'); ?>" placeholder="wpcf7_uploads"></label><br>
						</fieldset>
					</td>
				</tr>
				<tr valign="top">
					<th scope="row"><?php _e('Auto Delete Files','dnd-upload-cf7'); ?></th>
					<td>
						<select name="drag_n_drop_delete_options">
							<option value="">-</option>
							<option value="1" <?php selected( get_option('drag_n_drop_delete_options'), '1' ); ?> >1 Hour</option>
							<option value="4" <?php selected( get_option('drag_n_drop_delete_options'), '4' ); ?>>4 Hours</option>
							<option value="8" <?php selected( get_option('drag_n_drop_delete_options'), '8' ); ?>>8 Hours</option>
							<option value="12" <?php selected( get_option('drag_n_drop_delete_options'), '12' ); ?>>12 Hours</option>
							<option value="16" <?php selected( get_option('drag_n_drop_delete_options'), '16' ); ?>>16 Hours</option>
							<option value="20" <?php selected( get_option('drag_n_drop_delete_options'), '20' ); ?>>20 Hours</option>
							<option value="24" <?php selected( get_option('drag_n_drop_delete_options'), '24' ); ?>>24 Hours</option>
							<option value="48" <?php selected( get_option('drag_n_drop_delete_options'), '48' ); ?>>48 Hours (2Days)</option>
							<option value="96" <?php selected( get_option('drag_n_drop_delete_options'), '96' ); ?>>96 Hours (4Days)</option>
							<option value="120" <?php selected( get_option('drag_n_drop_delete_options'), '120' ); ?>>120 Hours (5Days)</option>
						</select>
						<span class="description"><?php _e('after submission','dnd-upload-cf7'); ?></span>
					</td>
				</tr>
				<tr valign="top">
					<th scope="row"><?php _e('Show image preview','dnd-upload-cf7'); ?></th>
					<td><input name="drag_n_drop_show_img_preview" <?php checked( get_option('drag_n_drop_show_img_preview'), 1 ); ?> type="checkbox" value="1"> Yes</td>
				</tr>
				<tr valign="top">
					<th scope="row"><?php _e('Zip Files','dnd-upload-cf7'); ?></th>
					<td><input name="drag_n_drop_zip_files" type="checkbox" <?php checked( get_option('drag_n_drop_zip_files'), 1 ); ?> value="1" <?php echo ( $zip_error ? 'disabled' : '' ); ?>> Yes <br>
					<?php echo ( $zip_error ? '<p class="description" style="color:red;">'. $zip_error . '</p>'  : '' ); ?></td>
				</tr>
			</table>

			<?php submit_button(); ?>
		</form>
	</div>

	<style>
		.conditional-show { display:none; }
		.conditional-show.show { display:table-row; }
	</style>
	<script type="text/javascript">
		jQuery(document).ready(function($){
			var $_conditional_field = $('.conditional-show');
			$('.conditional-field').click(function(){
				$_conditional_field.removeClass('show');
				$_conditional_field.find('[name="drag_n_drop_folder_option"]').attr('disabled', true);
				if( $(this).is(':checked') && ! $('.conditional-field-media').is(':checked') ) {
					$_conditional_field.addClass('show');
					$_conditional_field.find('[name="drag_n_drop_folder_option"]').removeAttr('disabled');
				}
			});
			$('.conditional-field-media').click(function(){
				if( $(this).is(':checked') && $('.conditional-field').is(':checked') ) {
					$_conditional_field.removeClass('show');
					$_conditional_field.find('[name="drag_n_drop_folder_option"]').attr('disabled', true);
				}else {
					if( $('.conditional-field').is(':checked') ) {
						$_conditional_field.addClass('show');
						$_conditional_field.find('[name="drag_n_drop_folder_option"]').removeAttr('disabled');
					}
				}
			})
		});
	</script>