<?php

	// if uninstall.php is not called by WordPress, die
	if (!defined('WP_UNINSTALL_PLUGIN')) {
		die;
	}

	// Lists of all options
	$options = array(
		'drag_n_drop_mail_attachment',
		'drag_n_drop_text',
		'drag_n_drop_separator',
		'drag_n_drop_browse_text',
		'drag_n_drop_error_server_limit',
		'drag_n_drop_error_failed_to_upload',
		'drag_n_drop_error_files_too_large',
		'drag_n_drop_error_invalid_file',
		'drag_n_drop_one_file_at_time',
		'drag_n_drop_save_to_media',
		'drag_n_drop_folder_option',
		'drag_n_drop_custom_folder',
		'drag_n_drop_delete_options',
		'drag_n_drop_show_img_preview',
		'drag_n_drop_zip_files'
	);

	// Loop and delete options
	foreach( $options as $option ) {
		delete_option( $option );
	}